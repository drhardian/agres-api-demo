<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'nama' => 'required|string|min:3|max:150',
            'sku' => 'required|min:10',
            'brand' => 'required|min:3|max:50',
            'deskripsi' => 'required|max:255',
            'harga' => 'required|numeric'
        ];
    }

    public function messages()
    {
        return [
            'nama.required' => 'Nama barang tidak boleh kosong',
            'nama.string' => 'Nama barang harus berupa text',
            'nama.min' => 'Nama barang minimal memiliki 3 karakter',
            'nama.max' => 'Nama barang maksimal memiliki 150 karakter',
            'sku.required' => 'Kode SKU tidak boleh kosong',
            'sku.min' => 'Kode SKU minimal memiliki 10 karakter',
            'brand.required' => 'Brand tidak boleh kosong',
            'brand.min' => 'Brand minimal memiliki 3 karakter',
            'brand.max' => 'Brand maksimal memiliki 50 karakter',
            'deskripsi.required' => 'Deskripsi barang tidak boleh kosong',
            'deskripsi.max' => 'Deskripsi barang maksimal memiliki 255',
            'harga.required' => 'Harga barang tidak boleh kosong',
            'harga.numeric' => 'Harga barang harus berupa angka'
        ];
    }
}
