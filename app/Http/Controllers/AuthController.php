<?php

namespace App\Http\Controllers;

use App\Http\Requests\LoginRequest;
use App\Http\Requests\RegisterRequest;
use App\Models\User;
use Exception;
use Illuminate\Support\Facades\Log;

class AuthController extends Controller
{
    # Memanggil middleware api untuk membatasi akses (tanpa otorisasi) ke seluruh fungsi didalam controller, 
    # fungsi yang dibatasi hanya fungsi login dan registrasi
    function __construct()
    {
        $this->middleware('auth:api')->except(['login','register']);
    }

    # fungsi untuk melakukan proses login
    public function login(LoginRequest $request)
    {
        # mengambil variabel (username & password) dari request sebagai parameter untuk login
        $credentials = $request->only('username', 'password');

        try {
            if(! $token = auth()->attempt($credentials)) {
                return response()->json([
                    'status' => false,
                    'message' => 'Tidak memiliki otorisasi'
                ], 401);
            }

            return $this->responseToken($token);
        } catch (Exception $th) {
            Log::error($th->getMessage());

            return response()->json([ 
                'status' => false,
                'message' => 'Token failed' 
            ], 500);
        }
    }

    # fungsi untuk melakukan proses logout
    public function logout()
    {
        try {
            auth()->logout();
    
            return response()->json([ 
                'status' => true,
                'message' => 'Logout berhasil' 
            ], 200);
        } catch (\Exception $th) {
            Log::error($th->getMessage());

            return response()->json([ 
                'status' => false,
                'message' => 'Logout gagal' 
            ], 500);
        }
    }

    # fungsi untuk melakukan proses registrasi
    public function register(RegisterRequest $request, User $user)
    {
        try {
            # menambahkan record user baru ke dalam table users
            $user->create([
                'name' => $request->name,
                'email' => $request->email,
                'username' => $request->username,
                'password' => bcrypt($request->password)
            ]);

            return response()->json([ 
                'status' => true,
                'message' => 'Registrasi berhasil' 
            ], 200);
        } catch (Exception $th) {
            Log::error($th->getMessage());

            return response()->json([ 
                'status' => false,
                'message' => 'Terjadi kesalahan pada server' 
            ], 500);
        }
    }

    # fungsi untuk membuat token baru
    public function refresh()
    {
        return $this->responseToken(auth()->refresh());
    }

    # fungsi untuk mengembalikan response dengan disertai value token, tipenya, dan masa aktif token
    public function responseToken($token)
    {
        return response()->json([
            'token' => $token,
            'type' => 'bearer',
            'expires_in' => auth()->factory()->getTTL() * 1
        ]);
    }
}
