<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreProductRequest;
use App\Http\Resources\ProductResource;
use App\Models\Product;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class ProductController extends Controller
{
    # Memanggil middleware api untuk membatasi akses (tanpa otorisasi) ke seluruh fungsi didalam controller
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    # Fungsi untuk menampilkan seluruh daftar produk
    public function index()
    {
        $products = Product::all();

        return response()->json([
            'status' => true,
            'products' => ProductResource::collection($products)
        ], 200);
    }

    # Fungsi untuk menyimpan produk baru
    public function store(StoreProductRequest $request, Product $product)
    {
        DB::beginTransaction();

        try {
            # Menyimpan ke dalam tabel products
            $product->create($request->validated());

            DB::commit();

            return response()->json([ 
                'status' => true,
                'message' => 'Produk baru berhasil ditambahkan' 
            ], 200);
        } catch (\Exception $th) {
            DB::rollBack();
            Log::error($th->getMessage());

            return response()->json([ 
                'status' => false,
                'message' => 'Terjadi kesalahan pada server' 
            ], 500);
        }
    }

    # Fungsi untuk menghapus produk
    public function destroy(Product $product)
    {
        DB::beginTransaction();

        try {
            # Menghapus record product dari tabel products sesuai id
            $product->delete();

            DB::commit();

            return response()->json([ 
                'status' => true,
                'message' => 'Product berhasil di hapus' 
            ], 200);
        } catch (\Exception $th) {
            DB::rollBack();
            Log::error($th->getMessage());

            return response()->json([ 
                'status' => false,
                'message' => 'Terjadi kesalahan pada server' 
            ], 500);
        }
    }
}
