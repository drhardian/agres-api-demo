<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ProductResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'nama' => $this->nama,
            'sku' => $this->sku,
            'brand' => $this->brand,
            'deskripsi' => $this->deskripsi,
            'variasi' => array($this->nama, $this->sku, $this->harga)
            // 'variasi' => array($this->only('nama','sku'))
        ];
    }
}
